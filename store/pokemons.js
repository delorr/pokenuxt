export const state = () => ({
  count: 0,
  pokemons: []
})
export const getters = {
  pokemons: state => {
    return state.pokemons
  },
  pokemonById: state => pokemonId => {
    return state.pokemons.find(todo => todo.id === pokemonId);
  }
}

export const mutations = {
  addToTeam(state, pokemon) {
    state.pokemons.push({
      id: pokemon.id,
      name: pokemon.name,
      sprite: pokemon.sprite
    })
    state.count++
  },
  removeToTeam(state, id) {
    const pokemonId = state.pokemons.find(pokemon => pokemon.id === id);
    const pokemonIndex = state.pokemons.indexOf(
      pokemonId
    );
    state.pokemons.splice(pokemonIndex, 1);
    state.count--
  }
}
