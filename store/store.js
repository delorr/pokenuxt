import Vue from 'vue';
import Vuex from 'vuex';

Vue.use(Vuex);

export const store = new Vuex.Store({
  state: {
    count: 0,
    pokemons: []
  },
  actions: {
    addToTeam({ commit }, pokemon) {
      commit("ADD_TO_TEAM", pokemon);
  },
  removeToTeam({ commit }, id) {
    commit("_TO_TEAM", id);
},
  },
  mutations: {
    addToTeam(state, pokemon) {
      console.log('beforeadd', pokemon)
      state.pokemons.push({
        id: pokemon.id,
        name: pokemon.name,
        sprite: pokemon.sprite
      })
    },
    removeToTeam(state, id) {
      const pokemonIndex = state.pokemons.indexOf(
				store.getters.pokemonById(id)
			);
			state.pokemons.splice(id, 1);
			delete state.pokemons[pokemonIndex].sprite;
      delete state.pokemons[pokemonIndex].sprite;
    }
  },
  getters: {
    pokemons: state => {
      return state.pokemons
    },
    pokemonById: state => pokemonId => {
      return state.pokemons.find(todo => todo.id === pokemonId);
    }
  }
})