import { instance } from '@/config/axios';

export default {
  getAllPokemons: (offset) => instance.get(`/pokemon?limit=20&offset=${offset}`),
  getPokemon: (name) => instance.get(`/pokemon/${name}`),
};